﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WcfService3.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Cliente</title>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script>
        var Buscar = function () {
            var urls = 'Service1.svc/mostrarUsuarios/';
            var datas = id.value;
            $.ajax({
                type: "GET",
                url: urls + datas,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data == null) {
                        alert('No se encontraron datos');
                        return true;
                    }
                    var result = data.GetEmployeeJSONResult;
                    var id = result.Id;
                    var name = result.nombre;

                    alert('Id: ' + id + ' Nombre: ' + name);
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });
        };

        var Guardar = function () {
            var urls = 'Service1.svc/NuevoUsuario/';
            var datas = id.value+'/'+nombre.value;
            $.ajax({
                type: "GET",
                url: urls + datas,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                //data: "{'Id':'" + id.value + "','nombre': '" + nombre.value + "'}",
                success: function (data) {
                    if (data == null) {
                        alert('No fue posible guardar el dato');
                        return true;
                    }

                    if (data == "yes") {
                        alert('Dato guardado correctamente');
                        return true;
                    }

                    if (data == "no") {
                        alert('No fue posible guardar el dato');
                        return true;
                    }

                    var result = data.GetEmployeeJSONResult;
                    var id = result.Id;
                    var name = result.nombre;

                    alert('Id: ' + id + ' Nombre: ' + name);

                    //$('#jsonData').html('');
                    //$('#jsonData').append('<table border="1"><tbody><tr><th>' +
                    //  "Employee Id</th><th>Name</th><th>Salary</th>" +
                    //  '</tr><tr><td>' + id + '</td><td>' + name +
                    //'</td><td>' + salary + '</td></tr></tbody></table>');

                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });
        }

        var LlamarServicio = function () {
            
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%--<asp:Button Text="Hola" runat="server" ID="btnBuscar" OnClientClick="Buscar()" />--%>
            Id
            <input type="number" id="id" name="id" /><br />
            Nombre
            <input type="text" id="nombre" name="nombre" />
            <br />
            <input type="button" value="Buscar 1" onclick="Buscar();" />
            <input type="button" value="Guardar" onclick="Guardar();" />
        </div>
    </form>
</body>
</html>

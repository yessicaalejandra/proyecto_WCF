﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;


namespace WcfService3
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        //[OperationContract]
       // List<usuario> mostrarusuarios();

      //  [OperationContract]
        //usuario Usuario(int id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarUsuarios", ResponseFormat = WebMessageFormat.Json)]
        List<usuario> mostrarUsuarios();

        [WebInvoke(Method = "GET", UriTemplate = "mostrarUsuarios/{Id}", ResponseFormat = WebMessageFormat.Json)]
        usuario mostrarUsuario(string Id);

        [WebInvoke(Method = "GET", UriTemplate = "GetUsuario1/{Id}", ResponseFormat = WebMessageFormat.Json)]
        usuario GetUsuario1(string Id);

        [WebInvoke(Method = "GET", UriTemplate = "NuevoUsuario/{Id}/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        string NuevoUsuario(string Id, string nombre);



        // TODO: agregue aquí sus operaciones de servicio
    }
}

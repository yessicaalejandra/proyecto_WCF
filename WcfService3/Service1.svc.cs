﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService3
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        private static Model1Container db = new Model1Container();

        // public List<usuario> mostrarUsuarios()
        //{
        //  return db.usuarioSet.ToList();
        // }

        // public usuario Usuario(int Id)
        //{
        //return db.usuarioSet.Where(p => p.Id == Id).First();
        //}


        public List<usuario> mostrarUsuarios()
        {
            return db.usuarioSet.ToList();
        }

        public usuario mostrarUsuario(string Id)
        {
            try
            {
                int nId = Convert.ToInt16(Id);


                usuario s=null;
                s = db.usuarioSet.Where(p => p.Id == nId).First();
                return s;

                //foreach (usuario row in db.usuarioSet)
                //    if (row.Id == nId) { 
                //        s = row;
                //        break;
                //    }
                //return s;
            }
            catch { return null; }
        }

        public usuario GetUsuario1(string Id)
        {
            return new usuario { Id = 1, nombre = "juan" };
        }

        public string NuevoUsuario(string Id, string nombre)
        {
            try
            {
                db.usuarioSet.Add(new usuario { Id = Convert.ToInt32(Id), nombre = nombre });
                return "yes";
            }
            catch
            {
                return "no";
            }
        }
    }
}

